
addRandomTetromino:
        addiu   $sp,$sp,-48
        sw      $31,44($sp)
        sw      $fp,40($sp)
        move    $fp,$sp
        sw      $4,48($fp)
        lw      $2,48($fp)
        nop
        lw      $3,4($2)
        lw      $2,48($fp)
        nop
        lw      $2,8($2)
        nop
        mult    $3,$2
        mflo    $2
        sll     $2,$2,1
        sll     $3,$2,2
        addu    $2,$2,$3
        sw      $2,24($fp)
$L3:
        jal     random
        nop

        move    $3,$2
        lw      $2,48($fp)
        nop
        lw      $2,4($2)
        nop
        bne     $2,$0,1f
        div     $0,$3,$2
        break   7
        mfhi    $2
        sw      $2,28($fp)
        jal     random
        nop

        move    $3,$2
        lw      $2,48($fp)
        nop
        lw      $2,8($2)
        nop
        bne     $2,$0,1f
        div     $0,$3,$2
        break   7
        mfhi    $2
        sw      $2,32($fp)
        lw      $2,24($fp)
        nop
        bgtz    $2,$L2
        nop

        lui     $2,%hi(__PRETTY_FUNCTION__.2471)
        addiu   $7,$2,%lo(__PRETTY_FUNCTION__.2471)
        li      $6,36                 # 0x24
        lui     $2,%hi($LC0)
        addiu   $5,$2,%lo($LC0)
        lui     $2,%hi($LC1)
        addiu   $4,$2,%lo($LC1)
        jal     __assert_fail
        nop

$L2:
        lw      $2,24($fp)
        nop
        addiu   $2,$2,-1
        sw      $2,24($fp)
        lw      $2,48($fp)
        nop
        lw      $3,0($2)
        lw      $2,32($fp)
        nop
        sll     $2,$2,2
        addu    $2,$3,$2
        lw      $3,0($2)
        lw      $2,28($fp)
        nop
        sll     $2,$2,2
        addu    $2,$3,$2
        lw      $3,0($2)
        li      $2,-2                 # 0xfffffffffffffffe
        beq     $3,$2,$L3
        nop

        lw      $2,48($fp)
        nop
        lw      $3,0($2)
        lw      $2,32($fp)
        nop
        sll     $2,$2,2
        addu    $2,$3,$2
        lw      $3,0($2)
        lw      $2,28($fp)
        nop
        sll     $2,$2,2
        addu    $2,$3,$2
        li      $3,-2                 # 0xfffffffffffffffe
        sw      $3,0($2)
        nop
        move    $sp,$fp
        lw      $31,44($sp)
        lw      $fp,40($sp)
        addiu   $sp,$sp,48
        j       $31
        nop

makeBoard:
        addiu   $sp,$sp,-64
        sw      $31,60($sp)
        sw      $fp,56($sp)
        sw      $16,52($sp)
        move    $fp,$sp
        sw      $4,64($fp)
        sw      $5,68($fp)
        sw      $6,72($fp)
        li      $4,16                 # 0x10
        jal     malloc
        nop

        sw      $2,40($fp)
        lw      $2,40($fp)
        nop
        bne     $2,$0,$L5
        nop

        lui     $2,%hi($LC2)
        addiu   $4,$2,%lo($LC2)
        jal     perror
        nop

        li      $4,1                        # 0x1
        jal     exit
        nop

$L5:
        lw      $2,68($fp)
        nop
        sll     $2,$2,2
        move    $4,$2
        jal     malloc
        nop

        move    $3,$2
        lw      $2,40($fp)
        nop
        sw      $3,0($2)
        lw      $2,40($fp)
        nop
        lw      $2,0($2)
        nop
        bne     $2,$0,$L6
        nop

        lui     $2,%hi($LC2)
        addiu   $4,$2,%lo($LC2)
        jal     perror
        nop

        li      $4,1                        # 0x1
        jal     exit
        nop

$L6:
        sw      $0,24($fp)
        b       $L7
        nop

$L9:
        lw      $2,40($fp)
        nop
        lw      $3,0($2)
        lw      $2,24($fp)
        nop
        sll     $2,$2,2
        addu    $16,$3,$2
        lw      $2,64($fp)
        nop
        sll     $2,$2,2
        move    $4,$2
        jal     malloc
        nop

        sw      $2,0($16)
        lw      $2,40($fp)
        nop
        lw      $3,0($2)
        lw      $2,24($fp)
        nop
        sll     $2,$2,2
        addu    $2,$3,$2
        lw      $2,0($2)
        nop
        bne     $2,$0,$L8
        nop

        lui     $2,%hi($LC2)
        addiu   $4,$2,%lo($LC2)
        jal     perror
        nop

        li      $4,1                        # 0x1
        jal     exit
        nop

$L8:
        lw      $2,24($fp)
        nop
        addiu   $2,$2,1
        sw      $2,24($fp)
$L7:
        lw      $3,24($fp)
        lw      $2,68($fp)
        nop
        slt     $2,$3,$2
        bne     $2,$0,$L9
        nop

        lw      $2,40($fp)
        lw      $3,64($fp)
        nop
        sw      $3,4($2)
        lw      $2,40($fp)
        lw      $3,68($fp)
        nop
        sw      $3,8($2)
        lw      $2,40($fp)
        lw      $3,72($fp)
        nop
        sw      $3,12($2)
        sw      $0,28($fp)
        b       $L10
        nop

$L13:
        sw      $0,32($fp)
        b       $L11
        nop

$L12:
        lw      $2,40($fp)
        nop
        lw      $3,0($2)
        lw      $2,28($fp)
        nop
        sll     $2,$2,2
        addu    $2,$3,$2
        lw      $3,0($2)
        lw      $2,32($fp)
        nop
        sll     $2,$2,2
        addu    $2,$3,$2
        li      $3,-1                 # 0xffffffffffffffff
        sw      $3,0($2)
        lw      $2,32($fp)
        nop
        addiu   $2,$2,1
        sw      $2,32($fp)
$L11:
        lw      $3,32($fp)
        lw      $2,64($fp)
        nop
        slt     $2,$3,$2
        bne     $2,$0,$L12
        nop

        lw      $2,28($fp)
        nop
        addiu   $2,$2,1
        sw      $2,28($fp)
$L10:
        lw      $3,28($fp)
        lw      $2,68($fp)
        nop
        slt     $2,$3,$2
        bne     $2,$0,$L13
        nop

        lw      $2,72($fp)
        nop
        bgez    $2,$L14
        nop

        lui     $2,%hi(__PRETTY_FUNCTION__.2492)
        addiu   $7,$2,%lo(__PRETTY_FUNCTION__.2492)
        li      $6,68                 # 0x44
        lui     $2,%hi($LC0)
        addiu   $5,$2,%lo($LC0)
        lui     $2,%hi($LC3)
        addiu   $4,$2,%lo($LC3)
        jal     __assert_fail
        nop

$L14:
        sw      $0,36($fp)
        b       $L15
        nop

$L16:
        lw      $4,40($fp)
        jal     addRandomMine
        nop

        lw      $2,36($fp)
        nop
        addiu   $2,$2,1
        sw      $2,36($fp)
$L15:
        lw      $3,36($fp)
        lw      $2,72($fp)
        nop
        slt     $2,$3,$2
        bne     $2,$0,$L16
        nop

        lw      $2,40($fp)
        move    $sp,$fp
        lw      $31,60($sp)
        lw      $fp,56($sp)
        lw      $16,52($sp)
        addiu   $sp,$sp,64
        j       $31
        nop

checkcollision:
        addiu   $sp,$sp,-72
        sw      $31,68($sp)
        sw      $fp,64($sp)
        move    $fp,$sp
        sw      $4,72($fp)
        sw      $0,24($fp)
        lui     $2,%hi($LC4)
        addiu   $4,$2,%lo($LC4)
        jal     printf
        nop

        sw      $0,28($fp)
        b       $L19
        nop

$L20:
        lw      $3,28($fp)
        li      $2,10                 # 0xa
        bne     $2,$0,1f
        div     $0,$3,$2
        break   7
        mfhi    $2
        mflo    $2
        move    $5,$2
        lui     $2,%hi($LC5)
        addiu   $4,$2,%lo($LC5)
        jal     printf
        nop

        lw      $2,28($fp)
        nop
        addiu   $2,$2,1
        sw      $2,28($fp)
$L19:
        lw      $2,72($fp)
        nop
        lw      $3,4($2)
        lw      $2,28($fp)
        nop
        slt     $2,$2,$3
        bne     $2,$0,$L20
        nop

        lui     $2,%hi($LC6)
        addiu   $4,$2,%lo($LC6)
        jal     printf
        nop

        sw      $0,32($fp)
        b       $L21
        nop

$L22:
        lw      $3,32($fp)
        li      $2,10                 # 0xa
        bne     $2,$0,1f
        div     $0,$3,$2
        break   7
        mfhi    $2
        move    $5,$2
        lui     $2,%hi($LC5)
        addiu   $4,$2,%lo($LC5)
        jal     printf
        nop

        lw      $2,32($fp)
        nop
        addiu   $2,$2,1
        sw      $2,32($fp)
$L21:
        lw      $2,72($fp)
        nop
        lw      $3,4($2)
        lw      $2,32($fp)
        nop
        slt     $2,$2,$3
        bne     $2,$0,$L22
        nop

        lui     $2,%hi($LC7)
        addiu   $4,$2,%lo($LC7)
        jal     printf
        nop

        sw      $0,36($fp)
        b       $L23
        nop

$L24:
        li      $4,45                 # 0x2d
        jal     putchar
        nop

        lw      $2,36($fp)
        nop
        addiu   $2,$2,1
        sw      $2,36($fp)
$L23:
        lw      $2,72($fp)
        nop
        lw      $3,4($2)
        lw      $2,36($fp)
        nop
        slt     $2,$2,$3
        bne     $2,$0,$L24
        nop

        li      $4,10                 # 0xa
        jal     putchar
        nop

        sw      $0,40($fp)
        b       $L25
        nop

$L32:
        lw      $3,40($fp)
        li      $2,100                  # 0x64
        bne     $2,$0,1f
        div     $0,$3,$2
        break   7
        mfhi    $2
        move    $5,$2
        lui     $2,%hi($LC8)
        addiu   $4,$2,%lo($LC8)
        jal     printf
        nop

        sw      $0,44($fp)
        b       $L26
        nop

$L31:
        lw      $2,72($fp)
        nop
        lw      $3,0($2)
        lw      $2,40($fp)
        nop
        sll     $2,$2,2
        addu    $2,$3,$2
        lw      $3,0($2)
        lw      $2,44($fp)
        nop
        sll     $2,$2,2
        addu    $2,$3,$2
        lw      $3,0($2)
        li      $2,-3                 # 0xfffffffffffffffd
        bne     $3,$2,$L27
        nop

        li      $4,42                 # 0x2a
        jal     putchar
        nop

        lw      $2,24($fp)
        nop
        addiu   $2,$2,1
        sw      $2,24($fp)
        b       $L28
        nop

$L27:
        lw      $2,72($fp)
        nop
        lw      $3,0($2)
        lw      $2,40($fp)
        nop
        sll     $2,$2,2
        addu    $2,$3,$2
        lw      $3,0($2)
        lw      $2,44($fp)
        nop
        sll     $2,$2,2
        addu    $2,$3,$2
        lw      $2,0($2)
        nop
        bgez    $2,$L29
        nop

        li      $4,63                 # 0x3f
        jal     putchar
        nop

        b       $L28
        nop

$L29:
        lw      $2,72($fp)
        nop
        lw      $3,0($2)
        lw      $2,40($fp)
        nop
        sll     $2,$2,2
        addu    $2,$3,$2
        lw      $3,0($2)
        lw      $2,44($fp)
        nop
        sll     $2,$2,2
        addu    $2,$3,$2
        lw      $2,0($2)
        nop
        bne     $2,$0,$L30
        nop

        li      $4,32                 # 0x20
        jal     putchar
        nop

        b       $L28
        nop

$L30:
        lw      $2,72($fp)
        nop
        lw      $3,0($2)
        lw      $2,40($fp)
        nop
        sll     $2,$2,2
        addu    $2,$3,$2
        lw      $3,0($2)
        lw      $2,44($fp)
        nop
        sll     $2,$2,2
        addu    $2,$3,$2
        lw      $2,0($2)
        nop
        move    $5,$2
        lui     $2,%hi($LC5)
        addiu   $4,$2,%lo($LC5)
        jal     printf
        nop

$L28:
        lw      $2,44($fp)
        nop
        addiu   $2,$2,1
        sw      $2,44($fp)
$L26:
        lw      $2,72($fp)
        nop
        lw      $3,4($2)
        lw      $2,44($fp)
        nop
        slt     $2,$2,$3
        bne     $2,$0,$L31
        nop

        li      $4,10                 # 0xa
        jal     putchar
        nop

        lw      $2,40($fp)
        nop
        addiu   $2,$2,1
        sw      $2,40($fp)
$L25:
        lw      $2,72($fp)
        nop
        lw      $3,8($2)
        lw      $2,40($fp)
        nop
        slt     $2,$2,$3
        bne     $2,$0,$L32
        nop

        lui     $2,%hi($LC7)
        addiu   $4,$2,%lo($LC7)
        jal     printf
        nop

        sw      $0,48($fp)
        b       $L33
        nop

$L34:
        li      $4,45                 # 0x2d
        jal     putchar
        nop

        lw      $2,48($fp)
        nop
        addiu   $2,$2,1
        sw      $2,48($fp)
$L33:
        lw      $2,72($fp)
        nop
        lw      $3,4($2)
        lw      $2,48($fp)
        nop
        slt     $2,$2,$3
        bne     $2,$0,$L34
        nop

        li      $4,10                 # 0xa
        jal     putchar
        nop

        lui     $2,%hi($LC4)
        addiu   $4,$2,%lo($LC4)
        jal     printf
        nop

        sw      $0,52($fp)
        b       $L35
        nop

$L36:
        lw      $3,52($fp)
        li      $2,10                 # 0xa
        bne     $2,$0,1f
        div     $0,$3,$2
        break   7
        mfhi    $2
        mflo    $2
        move    $5,$2
        lui     $2,%hi($LC5)
        addiu   $4,$2,%lo($LC5)
        jal     printf
        nop

        lw      $2,52($fp)
        nop
        addiu   $2,$2,1
        sw      $2,52($fp)
$L35:
        lw      $2,72($fp)
        nop
        lw      $3,4($2)
        lw      $2,52($fp)
        nop
        slt     $2,$2,$3
        bne     $2,$0,$L36
        nop

        lui     $2,%hi($LC6)
        addiu   $4,$2,%lo($LC6)
        jal     printf
        nop

        sw      $0,56($fp)
        b       $L37
        nop

$L38:
        lw      $3,56($fp)
        li      $2,10                 # 0xa
        bne     $2,$0,1f
        div     $0,$3,$2
        break   7
        mfhi    $2
        move    $5,$2
        lui     $2,%hi($LC5)
        addiu   $4,$2,%lo($LC5)
        jal     printf
        nop

        lw      $2,56($fp)
        nop
        addiu   $2,$2,1
        sw      $2,56($fp)
$L37:
        lw      $2,72($fp)
        nop
        lw      $3,4($2)
        lw      $2,56($fp)
        nop
        slt     $2,$2,$3
        bne     $2,$0,$L38
        nop

        lw      $2,72($fp)
        nop
        lw      $2,12($2)
        nop
        move    $6,$2
        lw      $5,24($fp)
        lui     $2,%hi($LC9)
        addiu   $4,$2,%lo($LC9)
        jal     printf
        nop

        nop
        move    $sp,$fp
        lw      $31,68($sp)
        lw      $fp,64($sp)
        addiu   $sp,$sp,72
        j       $31
        nop

countScore:
        addiu   $sp,$sp,-24
        sw      $fp,20($sp)
        move    $fp,$sp
        sw      $4,24($fp)
        sw      $5,28($fp)
        sw      $6,32($fp)
        sw      $0,8($fp)
        lw      $2,28($fp)
        nop
        bne     $2,$0,$L40
        nop

        lw      $2,32($fp)
        nop
        bne     $2,$0,$L41
        nop

        lw      $2,24($fp)
        nop
        lw      $3,0($2)
        lw      $2,32($fp)
        nop
        addiu   $2,$2,1
        sll     $2,$2,2
        addu    $2,$3,$2
        lw      $3,0($2)
        lw      $2,28($fp)
        nop
        addiu   $2,$2,1
        sll     $2,$2,2
        addu    $2,$3,$2
        lw      $3,0($2)
        li      $2,-2                 # 0xfffffffffffffffe
        beq     $3,$2,$L42
        nop

        lw      $2,24($fp)
        nop
        lw      $3,0($2)
        lw      $2,32($fp)
        nop
        addiu   $2,$2,1
        sll     $2,$2,2
        addu    $2,$3,$2
        lw      $3,0($2)
        lw      $2,28($fp)
        nop
        addiu   $2,$2,1
        sll     $2,$2,2
        addu    $2,$3,$2
        lw      $3,0($2)
        li      $2,-3                 # 0xfffffffffffffffd
        bne     $3,$2,$L43
        nop

$L42:
        lw      $2,8($fp)
        nop
        addiu   $2,$2,1
        sw      $2,8($fp)
$L43:
        lw      $2,24($fp)
        nop
        lw      $3,0($2)
        lw      $2,32($fp)
        nop
        addiu   $2,$2,1
        sll     $2,$2,2
        addu    $2,$3,$2
        lw      $3,0($2)
        lw      $2,28($fp)
        nop
        sll     $2,$2,2
        addu    $2,$3,$2
        lw      $3,0($2)
        li      $2,-2                 # 0xfffffffffffffffe
        beq     $3,$2,$L44
        nop

        lw      $2,24($fp)
        nop
        lw      $3,0($2)
        lw      $2,32($fp)
        nop
        addiu   $2,$2,1
        sll     $2,$2,2
        addu    $2,$3,$2
        lw      $3,0($2)
        lw      $2,28($fp)
        nop
        sll     $2,$2,2
        addu    $2,$3,$2
        lw      $3,0($2)
        li      $2,-3                 # 0xfffffffffffffffd
        bne     $3,$2,$L45
        nop

$L44:
        lw      $2,8($fp)
        nop
        addiu   $2,$2,1
        sw      $2,8($fp)
$L45:
        lw      $2,24($fp)
        nop
        lw      $3,0($2)
        lw      $2,32($fp)
        nop
        sll     $2,$2,2
        addu    $2,$3,$2
        lw      $3,0($2)
        lw      $2,28($fp)
        nop
        addiu   $2,$2,1
        sll     $2,$2,2
        addu    $2,$3,$2
        lw      $3,0($2)
        li      $2,-2                 # 0xfffffffffffffffe
        beq     $3,$2,$L46
        nop

        lw      $2,24($fp)
        nop
        lw      $3,0($2)
        lw      $2,32($fp)
        nop
        sll     $2,$2,2
        addu    $2,$3,$2
        lw      $3,0($2)
        lw      $2,28($fp)
        nop
        addiu   $2,$2,1
        sll     $2,$2,2
        addu    $2,$3,$2
        lw      $3,0($2)
        li      $2,-3                 # 0xfffffffffffffffd
        bne     $3,$2,$L65
        nop

$L46:
        lw      $2,8($fp)
        nop
        addiu   $2,$2,1
        sw      $2,8($fp)
        b       $L65
        nop

$L41:
        lw      $2,24($fp)
        nop
        lw      $2,8($2)
        nop
        addiu   $3,$2,-1
        lw      $2,32($fp)
        nop
        bne     $3,$2,$L49
        nop

        lw      $2,24($fp)
        nop
        lw      $3,0($2)
        lw      $4,32($fp)
        li      $2,1073676288                 # 0x3fff0000
        ori     $2,$2,0xffff
        addu    $2,$4,$2
        sll     $2,$2,2
        addu    $2,$3,$2
        lw      $3,0($2)
        lw      $2,28($fp)
        nop
        addiu   $2,$2,1
        sll     $2,$2,2
        addu    $2,$3,$2
        lw      $3,0($2)
        li      $2,-2                 # 0xfffffffffffffffe
        beq     $3,$2,$L50
        nop

        lw      $2,24($fp)
        nop
        lw      $3,0($2)
        lw      $4,32($fp)
        li      $2,1073676288                 # 0x3fff0000
        ori     $2,$2,0xffff
        addu    $2,$4,$2
        sll     $2,$2,2
        addu    $2,$3,$2
        lw      $3,0($2)
        lw      $2,28($fp)
        nop
        addiu   $2,$2,1
        sll     $2,$2,2
        addu    $2,$3,$2
        lw      $3,0($2)
        li      $2,-3                 # 0xfffffffffffffffd
        bne     $3,$2,$L51
        nop

$L50:
        lw      $2,8($fp)
        nop
        addiu   $2,$2,1
        sw      $2,8($fp)
$L51:
        lw      $2,24($fp)
        nop
        lw      $3,0($2)
        lw      $4,32($fp)
        li      $2,1073676288                 # 0x3fff0000
        ori     $2,$2,0xffff
        addu    $2,$4,$2
        sll     $2,$2,2
        addu    $2,$3,$2
        lw      $3,0($2)
        lw      $2,28($fp)
        nop
        sll     $2,$2,2
        addu    $2,$3,$2
        lw      $3,0($2)
        li      $2,-2                 # 0xfffffffffffffffe
        beq     $3,$2,$L52
        nop

        lw      $2,24($fp)
        nop
        lw      $3,0($2)
        lw      $4,32($fp)
        li      $2,1073676288                 # 0x3fff0000
        ori     $2,$2,0xffff
        addu    $2,$4,$2
        sll     $2,$2,2
        addu    $2,$3,$2
        lw      $3,0($2)
        lw      $2,28($fp)
        nop
        sll     $2,$2,2
        addu    $2,$3,$2
        lw      $3,0($2)
        li      $2,-3                 # 0xfffffffffffffffd
        bne     $3,$2,$L53
        nop

$L52:
        lw      $2,8($fp)
        nop
        addiu   $2,$2,1
        sw      $2,8($fp)
$L53:
        lw      $2,24($fp)
        nop
        lw      $3,0($2)
        lw      $2,32($fp)
        nop
        sll     $2,$2,2
        addu    $2,$3,$2
        lw      $3,0($2)
        lw      $2,28($fp)
        nop
        addiu   $2,$2,1
        sll     $2,$2,2
        addu    $2,$3,$2
        lw      $3,0($2)
        li      $2,-2                 # 0xfffffffffffffffe
        beq     $3,$2,$L54
        nop

        lw      $2,24($fp)
        nop
        lw      $3,0($2)
        lw      $2,32($fp)
        nop
        sll     $2,$2,2
        addu    $2,$3,$2
        lw      $3,0($2)
        lw      $2,28($fp)
        nop
        addiu   $2,$2,1
        sll     $2,$2,2
        addu    $2,$3,$2
        lw      $3,0($2)
        li      $2,-3                 # 0xfffffffffffffffd
        bne     $3,$2,$L65
        nop

$L54:
        lw      $2,8($fp)
        nop
        addiu   $2,$2,1
        sw      $2,8($fp)
        b       $L65
        nop

$L49:
        lw      $2,24($fp)
        nop
        lw      $3,0($2)
        lw      $2,32($fp)
        nop
        addiu   $2,$2,1
        sll     $2,$2,2
        addu    $2,$3,$2
        lw      $3,0($2)
        lw      $2,28($fp)
        nop
        addiu   $2,$2,1
        sll     $2,$2,2
        addu    $2,$3,$2
        lw      $3,0($2)
        li      $2,-2                 # 0xfffffffffffffffe
        beq     $3,$2,$L56
        nop

        lw      $2,24($fp)
        nop
        lw      $3,0($2)
        lw      $2,32($fp)
        nop
        addiu   $2,$2,1
        sll     $2,$2,2
        addu    $2,$3,$2
        lw      $3,0($2)
        lw      $2,28($fp)
        nop
        addiu   $2,$2,1
        sll     $2,$2,2
        addu    $2,$3,$2
        lw      $3,0($2)
        li      $2,-3                 # 0xfffffffffffffffd
        bne     $3,$2,$L57
        nop

$L56:
        lw      $2,8($fp)
        nop
        addiu   $2,$2,1
        sw      $2,8($fp)
$L57:
        lw      $2,24($fp)
        nop
        lw      $3,0($2)
        lw      $2,32($fp)
        nop
        addiu   $2,$2,1
        sll     $2,$2,2
        addu    $2,$3,$2
        lw      $3,0($2)
        lw      $2,28($fp)
        nop
        sll     $2,$2,2
        addu    $2,$3,$2
        lw      $3,0($2)
        li      $2,-2                 # 0xfffffffffffffffe
        beq     $3,$2,$L58
        nop

        lw      $2,24($fp)
        nop
        lw      $3,0($2)
        lw      $2,32($fp)
        nop
        addiu   $2,$2,1
        sll     $2,$2,2
        addu    $2,$3,$2
        lw      $3,0($2)
        lw      $2,28($fp)
        nop
        sll     $2,$2,2
        addu    $2,$3,$2
        lw      $3,0($2)
        li      $2,-3                 # 0xfffffffffffffffd
        bne     $3,$2,$L59
        nop

$L58:
        lw      $2,8($fp)
        nop
        addiu   $2,$2,1
        sw      $2,8($fp)
$L59:
        lw      $2,24($fp)
        nop
        lw      $3,0($2)
        lw      $2,32($fp)
        nop
        sll     $2,$2,2
        addu    $2,$3,$2
        lw      $3,0($2)
        lw      $2,28($fp)
        nop
        addiu   $2,$2,1
        sll     $2,$2,2
        addu    $2,$3,$2
        lw      $3,0($2)
        li      $2,-2                 # 0xfffffffffffffffe
        beq     $3,$2,$L60
        nop

        lw      $2,24($fp)
        nop
        lw      $3,0($2)
        lw      $2,32($fp)
        nop
        sll     $2,$2,2
        addu    $2,$3,$2
        lw      $3,0($2)
        lw      $2,28($fp)
        nop
        addiu   $2,$2,1
        sll     $2,$2,2
        addu    $2,$3,$2
        lw      $3,0($2)
        li      $2,-3                 # 0xfffffffffffffffd
        bne     $3,$2,$L61
        nop

$L60:
        lw      $2,8($fp)
        nop
        addiu   $2,$2,1
        sw      $2,8($fp)
$L61:
        lw      $2,24($fp)
        nop
        lw      $3,0($2)
        lw      $4,32($fp)
        li      $2,1073676288                 # 0x3fff0000
        ori     $2,$2,0xffff
        addu    $2,$4,$2
        sll     $2,$2,2
        addu    $2,$3,$2
        lw      $3,0($2)
        lw      $2,28($fp)
        nop
        addiu   $2,$2,1
        sll     $2,$2,2
        addu    $2,$3,$2
        lw      $3,0($2)
        li      $2,-2                 # 0xfffffffffffffffe
        beq     $3,$2,$L62
        nop

        lw      $2,24($fp)
        nop
        lw      $3,0($2)
        lw      $4,32($fp)
        li      $2,1073676288                 # 0x3fff0000
        ori     $2,$2,0xffff
        addu    $2,$4,$2
        sll     $2,$2,2
        addu    $2,$3,$2
        lw      $3,0($2)
        lw      $2,28($fp)
        nop
        addiu   $2,$2,1
        sll     $2,$2,2
        addu    $2,$3,$2
        lw      $3,0($2)
        li      $2,-3                 # 0xfffffffffffffffd
        bne     $3,$2,$L63
        nop

$L62:
        lw      $2,8($fp)
        nop
        addiu   $2,$2,1
        sw      $2,8($fp)
$L63:
        lw      $2,24($fp)
        nop
        lw      $3,0($2)
        lw      $4,32($fp)
        li      $2,1073676288                 # 0x3fff0000
        ori     $2,$2,0xffff
        addu    $2,$4,$2
        sll     $2,$2,2
        addu    $2,$3,$2
        lw      $3,0($2)
        lw      $2,28($fp)
        nop
        sll     $2,$2,2
        addu    $2,$3,$2
        lw      $3,0($2)
        li      $2,-2                 # 0xfffffffffffffffe
        beq     $3,$2,$L64
        nop

        lw      $2,24($fp)
        nop
        lw      $3,0($2)
        lw      $4,32($fp)
        li      $2,1073676288                 # 0x3fff0000
        ori     $2,$2,0xffff
        addu    $2,$4,$2
        sll     $2,$2,2
        addu    $2,$3,$2
        lw      $3,0($2)
        lw      $2,28($fp)
        nop
        sll     $2,$2,2
        addu    $2,$3,$2
        lw      $3,0($2)
        li      $2,-3                 # 0xfffffffffffffffd
        bne     $3,$2,$L65
        nop

$L64:
        lw      $2,8($fp)
        nop
        addiu   $2,$2,1
        sw      $2,8($fp)
        b       $L65
        nop

$L40:
        lw      $2,24($fp)
        nop
        lw      $2,4($2)
        nop
        addiu   $3,$2,-1
        lw      $2,28($fp)
        nop
        bne     $3,$2,$L66
        nop

        lw      $2,32($fp)
        nop
        bne     $2,$0,$L67
        nop

        lw      $2,24($fp)
        nop
        lw      $3,0($2)
        lw      $2,32($fp)
        nop
        addiu   $2,$2,1
        sll     $2,$2,2
        addu    $2,$3,$2
        lw      $3,0($2)
        lw      $4,28($fp)
        li      $2,1073676288                 # 0x3fff0000
        ori     $2,$2,0xffff
        addu    $2,$4,$2
        sll     $2,$2,2
        addu    $2,$3,$2
        lw      $3,0($2)
        li      $2,-2                 # 0xfffffffffffffffe
        beq     $3,$2,$L68
        nop

        lw      $2,24($fp)
        nop
        lw      $3,0($2)
        lw      $2,32($fp)
        nop
        addiu   $2,$2,1
        sll     $2,$2,2
        addu    $2,$3,$2
        lw      $3,0($2)
        lw      $4,28($fp)
        li      $2,1073676288                 # 0x3fff0000
        ori     $2,$2,0xffff
        addu    $2,$4,$2
        sll     $2,$2,2
        addu    $2,$3,$2
        lw      $3,0($2)
        li      $2,-3                 # 0xfffffffffffffffd
        bne     $3,$2,$L69
        nop
